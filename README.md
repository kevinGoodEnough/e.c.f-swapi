# SWAPI Client

Une page qui interroge l'API ouverte de https://swapi.co/


## Running

Si PHP est disponible en ligne de commande, on peux tester en lançant la
commande suivante depuis la racine du projet:

```bash
php -S 127.0.0.1:8000
```

Si Python 2 est installé:
```bash
python2 -m SimpleHTTPServer 8000
```

Si c'est Python 3:
```bash
python3 -m http.server 8000
```
